Solutions:

Solution one:

Takes input from the input file.
This solution, sum using iteratively all the elements  and backtrack the elements in second loop.
Complexity: O(n)=n+n=2n


Solution Two:

Takes input from the input file.
This solution, created all the sub arrays using iteratively  and sum the sub arrays.
Complexity: O(n)=n*n=n^n

Solution Three:

Takes input from the input file.
This solution, created all the sub arrays using recursive and sum the sub arrays.

Complexity: O(n)=n*2^n.



