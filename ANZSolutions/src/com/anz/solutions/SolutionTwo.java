package com.anz.solutions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SolutionTwo {

	static int actualSum = 0;

	public static int buildSubArrays(int[] root, int length, int k) {
		int sum = actualSum;
		int[] pos = new int[length];
		int[] combo = new int[length];
		for (int i = 0; i < length; i++)
			combo[i] = root[0];

		while (true) {
			// output the current combinations:

			int sum2 = Arrays.stream(combo).sum();
			if (sum2 > sum && sum2 <= k) {
				sum = sum2;
			}
			// move on to the next combination:
			int place = length - 1;
			while (place >= 0) {
				if (++pos[place] == root.length) {
					pos[place] = 0;
					combo[place] = root[0];
					place--;
				} else {
					combo[place] = root[pos[place]];
					break;
				}
			}
			if (place < 0)
				break;
		}
		actualSum = sum;
		return sum;
	}

	public static void main(String[] args) throws java.lang.Exception {

		List<String> list = new ArrayList<String>();
		File file = new File("Input/input.txt");
		BufferedReader reader = null;

		
		    reader = new BufferedReader(new FileReader(file));
		    String text = null;

		    while ((text = reader.readLine()) != null) {
		        list.add(text);
		    }
		    reader.close();
		    int t=Integer.parseInt(list.get(0));
		    if(t < 1 || t >10)
		    {
		    	System.out.println("invalid input");
		    	return;
		    }
		    List<String[]> sumList = new ArrayList<>(); 
		    for(int count=1;count<list.size();count=count+t)
		    {
		    	String strArray[] = list.get(count).split(" ");
		    	sumList.add(strArray);
		    }
		    List<String[]> arryList= new ArrayList<>();
		    for(int count=2;count<list.size();count=count+t)
		    {
		    	String strArray[] = list.get(count).split(" ");
		    	arryList.add(strArray);
		    }
		 for(int count=0;count< sumList.size();count=count+1)
		 {
			 int n = Integer.parseInt(sumList.get(count)[0]);
			 int k = Integer.parseInt(sumList.get(count)[1]);
			 int[] arr = new int[n];
			 if(arryList.get(count).length < 1)
			 {
				 System.out.println("invalid input");
					return; 
			 }
				for (int i = 0; i < n; i++) {
					int arrItem = Integer.parseInt(arryList.get(count)[i]);
					if(arrItem >2000)
					{
						System.out.println("invalid input");
						return;
					}
					arr[i] = arrItem;
				}
				actualSum = 0;
				int result = unboundedKnapsack(k, arr);
				System.out.println(result);
		 }	
	}

	private static int unboundedKnapsack(int k, int[] arr) {
		// TODO Auto-generated method stub
		ArrayList<Integer> maxSum = new ArrayList<>();
		int legth = arr.length;
		for (int i = 1; i <= legth; i++) {
			maxSum.add(buildSubArrays(arr, i, k));
		}
		return Collections.max(maxSum);
	}
}
