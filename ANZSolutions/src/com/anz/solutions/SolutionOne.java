package com.anz.solutions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SolutionOne {
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		List<String> list = new ArrayList<String>();
		File file = new File("Input/input.txt");
		BufferedReader reader = null;

		
		    reader = new BufferedReader(new FileReader(file));
		    String text = null;

		    while ((text = reader.readLine()) != null) {
		        list.add(text);
		    }
		    reader.close();
		    int t=Integer.parseInt(list.get(0));
		    if(t < 1 || t >10)
		    {
		    	System.out.println("invalid input");
		    	return;
		    }
		    List<String[]> sumList = new ArrayList<>(); 
		    for(int count=1;count<list.size();count=count+t)
		    {
		    	String strArray[] = list.get(count).split(" ");
		    	sumList.add(strArray);
		    }
		    List<String[]> arryList= new ArrayList<>();
		    for(int count=2;count<list.size();count=count+t)
		    {
		    	String strArray[] = list.get(count).split(" ");
		    	arryList.add(strArray);
		    }
		 for(int count=0;count< sumList.size();count=count+1)
		 {
			 int n = Integer.parseInt(sumList.get(count)[0]);
			 int k = Integer.parseInt(sumList.get(count)[1]);
			 int[] arr = new int[n];
			 if(arryList.get(count).length < 1)
			 {
				 System.out.println("invalid input");
					return; 
			 }
				for (int i = 0; i < n; i++) {
					int arrItem = Integer.parseInt(arryList.get(count)[i]);
					if(arrItem >2000)
					{
						System.out.println("invalid input");
						return;
					}
					arr[i] = arrItem;
				}
			
				int result = unboundedKnapsack(k, arr);
				System.out.println(result);
		 }	
	}

	private static int unboundedKnapsack(int k, int[] arr) {
		// TODO Auto-generated method stub
		int[] sum= new int[k+1];
		 
		 sum[0]=1;
		 
		 for(int i=0;i<arr.length;i++)
		 {
		     for(int j=arr[i];j<=k;j++)
		        sum[j]+=sum[j-arr[i]]; 
		 }

		for(int i=k;i>=0;i--)
		    if(sum[i]!=0)
		        return i;
		 
		 return 0;
		}
		
	}
	



